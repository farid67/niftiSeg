#!/usr/bin/python3

import argparse
import cv2
import gc
import itertools
import keras
from keras.models import load_model
from keras import backend as K
import nibabel
from nibabel import processing
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from pyquaternion import Quaternion
from scipy.ndimage.interpolation import affine_transform
from scipy.ndimage.morphology import binary_opening, binary_closing, binary_dilation, generate_binary_structure
from scipy.ndimage.measurements import label
from subprocess import call
import sys
from tempfile import TemporaryDirectory
from tqdm import tqdm
import tensorflow as tf

#####################
# Tf Custom objects
##################### 

def cross_entropy_loss(y_true, y_pred):
    # Notation used from paper : Fully Convolutional Architectures for Multi-Class Segmentation in Chest Radiographs

    # manual computation of crossentropy
    # clk corresponds to the number of pixels belonging to the channel l in the batch k
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk = tf.clip_by_value(clk, 1., ck)
    _epsilon = tf.convert_to_tensor(1e-7, y_pred.dtype.base_dtype)
    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    return -tf.reduce_sum(ck/clk * tf.reduce_mean(y_true * tf.log(y_pred), axis=1), axis=1)

def cross_entropy_loss_masking(y_true, y_pred):
    # Notation used from paper : Fully Convolutional Architectures for Multi-Class Segmentation in Chest Radiographs

    # manual computation of crossentropy
    # clk corresponds to the number of pixels belonging to the channel l in the batch k
    # print (K.eval(tf.gather(y_true, 0, axis=2)))
    mask_bg = tf.less(tf.gather(y_true, 0, axis=2), 1)
    # print (K.eval(mask_bg))
    y_true = tf.boolean_mask(y_true, mask_bg)
    y_pred = tf.boolean_mask(y_pred, mask_bg)

    # print (K.eval(y_true))
    clk = tf.reduce_sum(y_true, axis=0)
    mask = tf.greater(clk, 0)

    # print ("mask ",K.eval(mask))
    clk = tf.boolean_mask(clk, mask)
    # print (K.eval(tf.boolean_mask(y_true, mask, axis=1)))
    y_true = tf.boolean_mask(y_true, mask, axis=1)
    y_pred = tf.boolean_mask(y_pred, mask, axis=1)

    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    weights = ck/clk
    # clk = tf.clip_by_value(clk, 1., ck)
    # print ("Total number of voxels :", K.eval(ck))
    # print ("Number of voxels per class :", K.eval(clk))
    _epsilon = tf.convert_to_tensor(1e-7, y_pred.dtype.base_dtype)
    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    return -tf.reduce_sum(weights * tf.reduce_mean(y_true * tf.log(y_pred), axis=0))

def dice_loss(y_true, y_pred):
    """
        Compute the dice loss
    """
    # clk corresponds to the number of pixels belonging to the channel l in the batch k
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk = tf.clip_by_value(clk, 1., ck)
    dice = 2 * tf.reduce_sum(y_true * y_pred, axis=1)/tf.reduce_sum(y_true + y_pred, axis=1)
    return -tf.reduce_sum(ck/clk * dice, axis=1)

def dice_acc(y_true, y_pred):
    """
        Compute the dice accuracy
    """
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk_clipped = tf.clip_by_value(clk, 1., ck)
    y_pred_arg_max = tf.one_hot(tf.argmax(y_pred, 2), y_pred.shape[2])
    intersection = tf.reduce_sum(tf.reduce_sum(y_true * y_pred_arg_max, axis = [1]), axis=0)
    dice = 2 * intersection / (tf.reduce_sum(y_true, axis=[0, 1]) +  tf.reduce_sum(y_pred_arg_max, axis = [0, 1]))
    # Nan on the dice correspond to case where we have neither gt nor prediction data so the union of the two sets is empty
    # We replace those case by a -1 on the dice computation
    dice = tf.where(tf.is_nan(dice), tf.zeros_like(dice), dice)
    unclipped_weights = 1./clk
    sum_weights =  tf.reduce_sum(tf.where(tf.is_inf(unclipped_weights), tf.zeros_like(unclipped_weights), unclipped_weights), axis=0)
    return tf.tensordot((1/clk_clipped), tf.where(tf.is_nan(dice), tf.zeros_like(dice), dice), 1) / sum_weights

def dice_acc_masking(y_true, y_pred):
    """
        Compute the dice accuracy by weighting the result (depending on the number of pixels per class) and masking the bg
    """
    # Mask BG
    mask_bg = tf.less(tf.gather(y_true, 0, axis=2), 1)
    y_true = tf.boolean_mask(y_true, mask_bg)
    y_pred = tf.boolean_mask(y_pred, mask_bg)
    clk = tf.reduce_sum(y_true, axis=0)

    # Mask labels with 0 pixels
    mask = tf.greater(clk, 0)
    clk = tf.boolean_mask(clk, mask)
    y_true = tf.boolean_mask(y_true, mask, axis=1)
    y_pred = tf.boolean_mask(y_pred, mask, axis=1)

    # Compute the dice
    y_pred_arg_max = tf.one_hot(tf.argmax(y_pred, 1), depth=tf.shape(y_pred)[1])
    intersection_per_class = tf.reduce_sum(y_true * y_pred_arg_max, 0)
    dice_per_class = 2*intersection_per_class/(tf.reduce_sum(y_true, axis=0) +  tf.reduce_sum(y_pred_arg_max, axis=0))

    # Apply the weights
    sum_weights = tf.reduce_sum(1/clk)
    global_dice = tf.reduce_sum(tf.tensordot((1/clk), dice_per_class, axes=1))/sum_weights
    return global_dice

##########################
# Utils
##########################

def rescale_affine(input_affine, voxel_dims=[1, 1, 1], target_center_coords= None):
    """
    This function uses a generic approach to rescaling an affine to arbitrary
    voxel dimensions. It allows for affines with off-diagonal elements by
    decomposing the affine matrix into u,s,v (or rather the numpy equivalents)
    and applying the scaling to the scaling matrix (s).
    https://github.com/nipy/nibabel/issues/670

    Parameters
    ----------
    input_affine : np.array of shape 4,4
        Result of nibabel.nifti1.Nifti1Image.affine
    voxel_dims : list
        Length in mm for x,y, and z dimensions of each voxel.
    target_center_coords: list of float
        3 numbers to specify the translation part of the affine if not using the same as the input_affine.

    Returns
    -------
    target_affine : 4x4matrix
        The resampled image.
    """
    # Initialize target_affine
    target_affine = input_affine.copy()
    # Decompose the image affine to allow scaling
    u,s,v = np.linalg.svd(target_affine[:3,:3],full_matrices=False)
    
    # Rescale the image to the appropriate voxel dimensions
    s = voxel_dims
    
    # Reconstruct the affine
    target_affine[:3,:3] = u @ np.diag(s) @ v

    # Set the translation component of the affine computed from the input
    # image affine if coordinates are specified by the user.
    if target_center_coords is not None:
        target_affine[:3,3] = target_center_coords
    return target_affine

def proba2OneHot(prediction, image_size=512, target="liver"):
    """
        Convert model prediction to binary output, 0 = background, 1 = target class
        Args :
            prediction (numpy.ndarray): network output prediction
            image_size (int): returned image size (default to 512)
            target (str): target class
        Returns:
            out (numpy.ndarray): binary output, shape (image_size, image_size, 1)
    """
    if target not in ["liver", "tumor"] :
        raise Exception("Target {} not available".format(target))
    n_classes = 3
    if target == "liver":
        n_classes = 1
    predicted_image = np.reshape(prediction, (image_size, image_size, n_classes))
    out = np.zeros(((image_size, image_size, 1)))
    if n_classes == 1:
        out[predicted_image[:, :, 0] > 0.5] = 1
    else: # target = "tumor" or "liver" : 
        out[predicted_image[:, :, 2] > 0.5] = 1
    return np.squeeze(out)

def transform_image(input_image, input_spacing, largest_spacing):
    """
        Transform the image to have the size and the spacing required by the network

        Args :
            input_image (numpy.ndarray): image to transform
            input_spacing (tuple): input image spacing
            largest_spacing (tuple): spacing used in images used to train the network
        Returns:
            out_image (numpy.ndarray): transformed image
    """
    spacing_scale = np.divide(largest_spacing, input_spacing)
    # print ("spacing scale : ", spacing_scale)
    transfo_matrix = np.eye(2) * spacing_scale
    output_shape = [int(np.ceil(input_image.shape[i]/spacing_scale[i])) for i in range(2)]
    out_image = affine_transform(input_image, transfo_matrix, output_shape=output_shape, order=0, mode="nearest")
    return out_image

def reshapeBorder(inputImg, borderValue, desired_size=(512, 512)):
    """
        Reshape inputImg to desired_sizeXdesired_size by either cropping
        the input if size > desired_size or padding with borderValue
    """
    top = 0
    bottom = 0
    left = 0
    right = 0
    size = inputImg.shape
    if size[0] < desired_size[0]:
        # padding
        delta_h = desired_size[0] - size[0]
        top, bottom = delta_h//2, delta_h-(delta_h//2)
    if size[1] < desired_size[1]:
        # padding
        delta_w = desired_size[1] - size[1]
        left, right = delta_w//2, delta_w-(delta_w//2)
    return cv2.resize(
        cv2.copyMakeBorder(
            inputImg, top, bottom, left, right, cv2.BORDER_CONSTANT, value=borderValue),
        (desired_size[1], desired_size[0]),
        interpolation=cv2.INTER_NEAREST)

def invertReshape(inputImg, desired_size):
    """
        Perfom the inverse operation of reshapeBorder, by going back to the desired_size.
        If desired_size in higher than current size, the image is resized, otherwise the image is cropped so the padding is removed
        (Need to invert the shape when calling cv2 since dimensions are interpreted differently)
        
        Args:
            input_image (numpy.ndarray): image to transform
            desired_size (tuple): target output image's size
        
        Returns:
            out_image (numpy.ndarray): output image either resized or cropped
    """
    size = inputImg.shape
    if size[1] > desired_size[1]:
        middle_h = size[1]//2
        left = middle_h - int(desired_size[1]/2)
        out_image = inputImg[:, left:left+desired_size[1]]
    else:
        out_image = cv2.resize(inputImg, (desired_size[1], size[0]),
                               interpolation=cv2.INTER_NEAREST)
    if size[0] > desired_size[0]:
        middle_v = size[0]//2
        top = middle_v - int(desired_size[0]/2)
        out_image = out_image[top:top+desired_size[0], :]
    else:
        out_image = cv2.resize(out_image, (desired_size[1], desired_size[0]),
                               interpolation=cv2.INTER_NEAREST)
    return out_image

def gridPatchesIdx(img_size, patch_size, grid_size):
    max_patches_per_line = img_size - (patch_size-1)
    patches_per_line = int(max_patches_per_line/grid_size)
    idx = []
    for j in range(patches_per_line):
        for i in range(patches_per_line):
            idx.append(j*grid_size*max_patches_per_line + i*grid_size)
        # last patch of the line
        idx.append(j*grid_size*max_patches_per_line + (max_patches_per_line-1))
    for i in range(patches_per_line):
        idx.append(max_patches_per_line * (max_patches_per_line-1) + i*grid_size)
    idx.append((max_patches_per_line+1) * (max_patches_per_line-1) )

    # last line?
    return idx

def patchToCoord(idx, img_size, patch_size):
    """
        Convert patch idx to image coordinates
    """
    number_of_patches_per_line = img_size-(patch_size-1)
    return idx%number_of_patches_per_line, int(idx/number_of_patches_per_line)

def mask(input_image, annotated_image):
    """
        Function to mask the input_image in order to fit with the network requirements
        (e.g. when segmenting the lesions, all non-liver pixels have to be masked)
        Args:
            input_image (numpy.ndarray): image to mask
            annotated_image (numpy.ndarray): binary image used to mask the input image, should normally correspond to previous segmentation in the cascade
        Returns:
            out_image (numpy.ndarray): The masked image
    """
    out_image = np.copy(input_image)
    out_image[annotated_image == 0] = -1
    return out_image

def fuseMasks(liverBinary, tumorBinary, necrosisBinary):
    """
        Create a multilabel output map with 4 classes, the background, the parenchyma, and both the active and the necrosis part of the lesions
        Args:
            liverBinary (numpy.ndarray): image corresponding to the binary segmentation of the liver
            tumorBinary (numpy.ndarray): image corresponding to the binary segmentation of the tumor
            necrosisBinary (numpy.ndarray): image corresponding to the binary segmentation of the necrosis
        Returns:
            out_image (numpy.ndarray): multilabel segmentation map
    """
    out = np.zeros((liverBinary.shape[0], liverBinary.shape[1], liverBinary.shape[2]))
    out[liverBinary == 1] = 1
    out[tumorBinary == 1] = 2
    out[necrosisBinary == 1] = 3
    return out

def find_nearest(array, value):
    """
    Return index of the nearest value present in an array
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def compute_hist(raw_volume, gt_volume, number_of_bins=500):
    try :
        pixels_set = raw_volume[np.where(gt_volume>0)]
        hist, _ = np.histogram(pixels_set, number_of_bins, [-100, 400], normed=True)
        return hist
    except:
        return None

def get_hist(hist_path):
    try:
        df = pd.read_csv(hist_path, index_col=[0])
        # TODO : Find a proper way to do it
        return df[str(0)] #if str(patient_id) in df.keys() else None
    except :
        return None

def get_mapping(set_in, cdf_in, cdf_target):
    mapping = {}
    for cdf_idx, cdf_el in enumerate(cdf_in) :
        mapping[set_in[cdf_idx]] = set_in[find_nearest(cdf_target, cdf_el)]
    return mapping

def pre_process_image(input_image, mapping, phase="VE", image_size=None):
    if phase == "VE":
        intensity_range = (20, 187)
    elif phase == "AR":
        intensity_range = (-5, 135)
    else:
        intensity_range = (-100, 399)
    if image_size == None:
        image_size = 512
    f_linFun = lambda x: (x-intensity_range[0])/(intensity_range[1]-intensity_range[0])
    out_image = cv2.resize(input_image, (image_size, image_size), interpolation= cv2.INTER_NEAREST)
    out_image = np.clip(out_image, intensity_range[0], intensity_range[1]).astype(np.int)
    if mapping is not None:
        out_image = np.vectorize(mapping.__getitem__)(out_image.flatten()).reshape((image_size, image_size))
    out_image = f_linFun(out_image)
    return out_image

##########################
# Main pred function
##########################
from matplotlib.colors import LinearSegmentedColormap
def segmentNifti(inputFiles, modelLiverPath=None, modelLiverPathPatch=None, modelTumorPath=None, modelTumorPatchPath=None, modelNecrosisPath=None,
    outputDir=None, fuse=False, fixLiver=True, two_5d_liver=False, two_5d_tumor=False, bg_input_masking=False, triplanar=False, mean_hist_files=[], custom_norm_range=False):
    """ 
        Convert nifti volume to numpy then run prediction (Liver, Tumor and Necrosis if specified as arg) and convert back to nifti by preserving geometrical properties
        Args:
            inputFile (str): path to the input image
            modelLiverPath (str): path to the model used to segment the liver within the whole CT
            modelTumorPath (str): path to the model used to segment the tumor within the liver (default to None)
            modelTumorPatchPath (str): path to the model used to segment the tumor within the liver, PATCHES version (default to None)
            modelNecrosisPath (str): path to the model used to segment the necrosis within the lesions (default to None)
            outputDir (str): path used as prefix to store the created nifti volume
        Returns :
            None : Create only the different nifti volumes
    """

    #Sentence to add to the predicted volume header
    prediction_nii_descr = "Volume segmented with niftiSeg https://gitlab.com/farid67/niftiSeg.git; \
        models liver : {}, tumor : {}, necrosis : {}".format(modelLiverPath, modelTumorPath, modelNecrosisPath).encode("ascii")
    custom_objects = {"cross_entropy_loss":cross_entropy_loss,
        "cross_entropy_loss_masking":cross_entropy_loss_masking,
        "dice_loss":dice_loss,
        "dice_acc": dice_acc,
        "dice_acc_masking":dice_acc_masking}
    nifti_data = [nibabel.load(inputFile) for inputFile in inputFiles]
    slice_number = nifti_data[0].shape[2]
    spacing_x, spacing_y, spacing_z = nifti_data[0].header.get("pixdim")[1:4]
    for nifti_data_ in nifti_data[1:] :
        if tuple(nifti_data_.header.get("pixdim")[1:3]) != (spacing_x, spacing_y):
            raise Exception("In-plane spacing is inconsistent among the different phases")
        if nifti_data_.shape[2] != slice_number:
            raise Exception("the number of slice is inconsistent among the different phases")

    pixel_array = [np.asanyarray(nifti_data_.dataobj) for nifti_data_ in nifti_data]
    for phase_idx, nifti_data_ in enumerate(nifti_data):
        if nifti_data_.header.has_data_slope and nifti_data_.header.has_data_intercept and nifti_data_.header.get_slope_inter() != (None, None):
            pixel_array[phase_idx] = pixel_array[phase_idx]*nifti_data_.header.get_slope_inter()[0] + nifti_data_.header.get_slope_inter()[1]

    rotation_matrix = Quaternion(nifti_data[0].header.get_qform_quaternion()).rotation_matrix

    liver_out_pixel_array = np.zeros(pixel_array[0].shape)

    ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            pixel_array = [np.flip(pixel_array_, axis=ax) for pixel_array_ in pixel_array]

    print ("Liver segmentation...\n")
    ### perform the liver segmentation here
    if modelLiverPath != None:
        model = load_model(modelLiverPath, custom_objects=custom_objects)
    else:
        model = load_model(modelLiverPathPatch, custom_objects=custom_objects)
    min_range = -100.
    max_range = 400.
    f_linFun = lambda x: (np.clip(x, min_range, max_range)-min_range)/(max_range-min_range)

    for slice_ in tqdm(range(pixel_array[0].shape[2])):
        if modelLiverPath != None:
            current_slices = [max(slice_-1, 0), slice_, min(slice_+1, pixel_array[0].shape[2]-1)] if two_5d_liver else [slice_]
            transformed_images = [[cv2.resize(reshapeBorder(transform_image(pixel_array_[:, :, s].T, (spacing_x, spacing_y), (0.68, 0.68)), borderValue=-1024, desired_size=(512, 512)),
                                              (int(model.input.shape[-1]), int(model.input.shape[-1])), interpolation=cv2.INTER_NEAREST) for s in current_slices]
                                              for pixel_array_ in pixel_array]
            transformed_images = list(itertools.chain.from_iterable(transformed_images)) # flatten lists since we have only either multiphase or 2.5 D for the moment
            if len(inputFiles) > 1 :
                transformed_images = [transformed_images[1]] # use only VE for the liver segmentation, since VE segmentation works better than MP
            X = np.array([f_linFun(transformed_image) for transformed_image in transformed_images])
            out = proba2OneHot(model.predict(X[np.newaxis, :, :, :]), image_size=model.input.shape[-1])
            X = None
        else:
            out = segmentPatchSlice(pixel_array[0][:,:, slice_], inputSpacing=(spacing_x, spacing_y), outputSpacing=(0.68, 0.68),
                model=model, n_classes=1)
            out = proba2OneHot(out)
        out = cv2.resize(out, (512, 512), interpolation=cv2.INTER_NEAREST)
        liver_out_pixel_array[:, :, slice_] = cv2.resize(
            transform_image(
                invertReshape(out, desired_size=(int(pixel_array[0].shape[0]*spacing_x/0.68), int(pixel_array[0].shape[1]*spacing_y/0.68))),
                (0.68, 0.68),
                (spacing_x, spacing_y)).T,
            (pixel_array[0].shape[1], pixel_array[0].shape[0]) , interpolation=cv2.INTER_NEAREST)
        out = None
        gc.collect()

    if triplanar:
        sagittal_liver_seg, coronal_liver_seg = segmentSagittalCoronal(inputFile,
            current_model=model,
            original_spacings=[spacing_x, spacing_y, spacing_z],
            axial_liver_segmentation=None,
            two_5d_liver = two_5d_liver,
            outputDir=outputDir)
    print ("\n")
    model = None
    gc.collect()
    ### end liver segmentation

    if fixLiver:
        liver_out_pixel_array_debug = np.copy(liver_out_pixel_array)
        ### Liver Segmentation Post-Processing
        struct = generate_binary_structure(3, 1)

        if triplanar:
            liver_out_pixel_array_fuse = np.copy(liver_out_pixel_array)
            # liver_out_pixel_array_fuse[np.where(np.logical_and(binary_dilation(liver_out_pixel_array, structure=struct, iterations=2).astype(np.int)==1,
            #     sagittal_liver_seg==1))] = 1
            # liver_out_pixel_array_fuse[np.where(np.logical_and(binary_dilation(liver_out_pixel_array, structure=struct, iterations=2).astype(np.int)==1,
            #     coronal_liver_seg==1))] = 1
            liver_out_pixel_array_fuse[np.where(np.logical_and(coronal_liver_seg==1,
                sagittal_liver_seg==1))] = 1
            liver_out_pixel_array = np.copy(liver_out_pixel_array_fuse)
            liver_out_pixel_array_fuse = None

        # Try here to perform a 3D opening of the obtained binary volume
        liver_out_pixel_array_opened = binary_opening(liver_out_pixel_array, structure=struct, iterations=1).astype(np.int)
        liver_out_pixel_array_debug[np.where(np.logical_and(liver_out_pixel_array == 1, liver_out_pixel_array_opened==0))] = 2 # Pixels labeled 2 correspond to those considered as liver and erased by the opening
        liver_out_pixel_array[np.where(np.logical_and(liver_out_pixel_array == 1, liver_out_pixel_array_opened==0))] = 0 # Pixels labeled 0 correspond to those considered as liver and erased by the opening

        # get the different 3D connected components and use only the biggest
        labeled, ncomponents = label(liver_out_pixel_array, structure=struct)
        _, counts = np.unique(labeled, return_counts=True)
        biggest_connected_component = np.argsort(counts)[-2] # Biggest connected component is the bg, the second might be the liver
        liver_out_pixel_array_debug[np.where(np.logical_and(liver_out_pixel_array == 1, labeled!=biggest_connected_component))] = 3 # Pixels labeled 3 correspond to those considered as liver and erased by the Connected component selection
        liver_out_pixel_array[np.where(np.logical_and(liver_out_pixel_array == 1, labeled!=biggest_connected_component))] = 0 # Pixels labeled 3 correspond to those considered as liver and erased by the Connected component selection

        labeled = None
        liver_out_pixel_array_opened = None
        coronal_liver_seg = None
        sagittal_liver_seg = None
        gc.collect()

    ### Perform additional segmentation (Tumor and necrosis if specified)
    if modelTumorPath != None or modelTumorPatchPath !=None:
        print ("Tumor segmentation...\n")
        tumor_out_pixel_array = np.zeros(pixel_array[0].shape)
        tumor_out_pixel_array_full_img = np.zeros(pixel_array[0].shape)
        tumor_out_pixel_array_patches = np.zeros(pixel_array[0].shape)

        if modelTumorPath != None:
            model = load_model(modelTumorPath, custom_objects=custom_objects)
        else:
            model = load_model(modelTumorPatchPath, custom_objects=custom_objects)

        # Get mapping if possible
        mapping = [None for i in range (len(pixel_array))]
        if len(mean_hist_files) != 0:
            for phase_idx, mean_hist_file in enumerate(mean_hist_files):
                mean_hist = get_hist(mean_hist_file)
                mean_cdf = mean_hist.cumsum()
                current_pat_hist = compute_hist(pixel_array[phase_idx],liver_out_pixel_array>0)
                current_pat_cdf = current_pat_hist.cumsum()
                mapping[phase_idx] = get_mapping(range(-100, 401), current_pat_cdf, mean_cdf)
            if two_5d_tumor:
                mapping.append(mapping[0])
                mapping.append(mapping[0])
        if len(pixel_array) == 1:
            # phases = [""]
            phases = ["AR"] if custom_norm_range else [""]
            if two_5d_tumor:
                phases = [phases[0] for i in range(3) ]
        else:
            phases = ["AR", "VE"] if custom_norm_range else ["", ""]

        for slice_ in tqdm(range(pixel_array[0].shape[2])):
            if modelTumorPath != None:
                img_size = int(model.input[0].shape[-1]) if bg_input_masking else int(model.input.shape[-1])
                target = "liver" if int(model.output.shape[-1]) == 1 else "tumor"
                current_slices = [max(slice_-1, 0), slice_, min(slice_+1, pixel_array[0].shape[2]-1)] if two_5d_tumor else [slice_]
                transformed_images = [[cv2.resize(reshapeBorder(transform_image(pixel_array_[:, :, s].T, (spacing_x, spacing_y), (0.68, 0.68)), borderValue=-1024, desired_size=(512, 512)),
                                              (img_size, img_size), interpolation=cv2.INTER_NEAREST) for s in current_slices]
                                              for pixel_array_ in pixel_array]
                transformed_images = list(itertools.chain.from_iterable(transformed_images)) # flatten lists since we have only either multiphase or 2.5 D for the moment
                transformed_annotated_images = [cv2.resize(reshapeBorder(transform_image(liver_out_pixel_array[:, :, s].T, (spacing_x, spacing_y), (0.68, 0.68)),
                                                                            borderValue=0,
                                                                            desired_size=(512, 512)),
                                                            (img_size,
                                                            img_size),
                                                            interpolation=cv2.INTER_NEAREST)
                                                for s in current_slices]
                if bg_input_masking:
                    X = np.array([f_linFun(transformed_images[0])])
                    X_mask = np.ones(transformed_images[0].shape)
                    X_mask [transformed_annotated_images[0] == 0] = 0
                    X_mask = np.array([X_mask])
                    out = proba2OneHot(model.predict([X[:, np.newaxis, :, :], X_mask[:, np.newaxis, :, :]]), image_size=img_size, target=target)
                    out[transformed_annotated_images[0]==0] = 0
                else:
                    X = np.array([mask(pre_process_image(transformed_images[idx], mapping=mapping[idx], phase=phases[idx], image_size=img_size),
                                       transformed_annotated_images[min(idx, len(transformed_annotated_images)-1)]) for idx in range(len(transformed_images))])
                    out = proba2OneHot(model.predict([X[np.newaxis, :, :, :]]), image_size=img_size, target=target)
                out = cv2.resize(out, (512, 512), interpolation=cv2.INTER_NEAREST)
                tumor_out_pixel_array_full_img[:, :, slice_] = cv2.resize(transform_image(invertReshape(out,
                                                                                               desired_size=(int(
                                                                                                   pixel_array[0].shape[0]*spacing_x/0.68), int(pixel_array[0].shape[1]*spacing_y/0.68))
                                                                                               ),
                                                                                 (0.68, 0.68), (spacing_x, spacing_y)).T, (pixel_array[0].shape[1], pixel_array[0].shape[0]),
                                                                 interpolation=cv2.INTER_NEAREST)
            if modelTumorPatchPath != None:
                inputImage = [pixel_array[:, :, s] for s in [max(slice_-1, 0), slice_, min(
                    slice_+1, pixel_array.shape[2]-1)]] if two_5d_tumor else [pixel_array_[:, :, slice_] for pixel_array_ in pixel_array]
                stride = 16
                toPredictPatch = gridPatchesIdx(512, patch_size=int(model.input.shape[-1]), grid_size=stride)
                overallPrediction, liver_out_pixel_array[:, :, slice_] = segmentPatchSlice(inputImage=inputImage, inputAnnotatedImage=liver_out_pixel_array[:, :, slice_],
                                                                                           inputSpacing=(spacing_x, spacing_y), outputSpacing=(0.68, 0.68), model=model, meanVoters=True,
                                                                                           n_classes=3, fixLiver=False, two_5d=two_5d_tumor, stride=stride, toPredictPatch=toPredictPatch, mapping=mapping)
                tumor_out_pixel_array_patches[:, :, slice_] = cv2.resize(transform_image(invertReshape(overallPrediction,
                                                                                                       desired_size=(int(
                                                                                                           pixel_array[0].shape[0]*spacing_x/0.68), int(pixel_array[0].shape[1]*spacing_y/0.68))
                                                                                                       ),
                                                                                         (0.68, 0.68), (spacing_x, spacing_y)).T, (pixel_array[0].shape[1], pixel_array[0].shape[0]),
                                                                         interpolation=cv2.INTER_NEAREST)

        # COMBINE FULLIMG-BASED AND PATCH-BASED
        if modelTumorPath != None and modelTumorPatchPath != None:
            tumor_out_pixel_array[np.logical_and(tumor_out_pixel_array_patches != 0, tumor_out_pixel_array_full_img == 1)] = 1 # Where full img network says 1 and patch says non bg
            tumor_out_pixel_array[tumor_out_pixel_array_patches == 2] = 1 # Where patch says tumor
        else:
            tumor_out_pixel_array = np.maximum(tumor_out_pixel_array_patches, tumor_out_pixel_array_full_img)

        if triplanar:
            sagittal_tumor_seg, coronal_tumor_seg = segmentSagittalCoronal(inputFile,
                current_model=model,
                original_spacings=[spacing_x, spacing_y, spacing_z],
                axial_liver_segmentation=liver_out_pixel_array,
                two_5d_liver = two_5d_liver,
                outputDir=outputDir)
            tumor_out_pixel_array_fuse = np.copy(tumor_out_pixel_array)
            tumor_out_pixel_array_fuse[np.where(np.logical_and(coronal_tumor_seg==1,
                sagittal_tumor_seg==1))] = 1
            tumor_out_pixel_array = np.copy(tumor_out_pixel_array_fuse)
            tumor_out_pixel_array_fuse = None
            coronal_tumor_seg = None
            sagittal_tumor_seg = None

        #Post-process
        struct = generate_binary_structure(3, 1)
        tumor_out_pixel_array = binary_opening(tumor_out_pixel_array, structure=struct, iterations=1).astype(np.int)
        # tumor_out_pixel_array[np.where(np.logical_and(tumor_out_pixel_array == 1, tumor_out_pixel_array_opened==0))] = 0


        print ("\n")
        model = None
        gc.collect()

        if modelNecrosisPath != None:
            print ("Necrosis segmentation...\n")
            necrosis_out_pixel_array = np.zeros(pixel_array[0].shape)

            model = load_model(modelNecrosisPath, custom_objects=custom_objects)

            for slice_ in tqdm(range(pixel_array[0].shape[2])):
                # sys.stdout.write("{}/{}\r".format(slice_+1,pixel_array.shape[2]))
                # sys.stdout.flush()

                transformed_image = cv2.resize(transform_image(pixel_array[:,:, slice_].T, (spacing_x, spacing_y), (0.97, 0.97)),
                    (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
                transformed_annotated_image = cv2.resize(transform_image(tumor_out_pixel_array[:,:, slice_].T, (spacing_x, spacing_y), (0.97, 0.97)),
                    (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
                X = np.array([mask(f_linFun(transformed_image), transformed_annotated_image)])
                out = proba2OneHot(model.predict(X[:, np.newaxis, :, :]), image_size=model.input.shape[-1], target="tumor")
                necrosis_out_pixel_array[:, :, slice_] = cv2.resize(transform_image(out, (0.97, 0.97), (spacing_x, spacing_y)).T, (pixel_array.shape[1], pixel_array.shape[0]) , interpolation=cv2.INTER_NEAREST)

            print ("\n")
            del model
            gc.collect()
    ### End / Perform additional segmentation (Tumor and necrosis if specified)


    ### retransform the output array, by transposing back, flipping, and rescale the data (slope&intercept)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            liver_out_pixel_array = np.flip(liver_out_pixel_array, axis=ax)
            if modelTumorPath != None or modelTumorPatchPath != None:
                tumor_out_pixel_array = np.flip(tumor_out_pixel_array, axis=ax)
            if modelNecrosisPath != None:
                necrosis_out_pixel_array = np.flip(necrosis_out_pixel_array, axis=ax)

    ### Create dir and files 
    if not os.path.exists(os.path.abspath(outputDir)):
        os.mkdir(os.path.abspath(outputDir))

    ### Get the input filename to have more specific output name
    fname = os.path.basename(inputFiles[0]).split(".")[0]

    original_header = nifti_data[0].header
    original_affine = nifti_data[0].affine
    if fuse : 
        out_nifti_data = nibabel.nifti1.Nifti1Image(fuseMasks(liver_out_pixel_array, tumor_out_pixel_array, necrosis_out_pixel_array),
                                                    affine=original_affine, header=original_header.copy())
        out_nifti_data.header.set_slope_inter(None, None)
        out_nifti_data.header["descrip"] = prediction_nii_descr
        nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_liverTissues.nii.gz"))
    else :
        ### Here convert numpy volume to nifti back
        out_nifti_data = nibabel.nifti1.Nifti1Image(liver_out_pixel_array, affine=original_affine, header=original_header.copy())
        out_nifti_data.header.set_slope_inter(None, None)
        out_nifti_data.header["descrip"] = prediction_nii_descr
        nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_liver.nii.gz"))
        if fixLiver:
            out_nifti_data = nibabel.nifti1.Nifti1Image(liver_out_pixel_array_debug, affine=original_affine, header=original_header.copy())
            out_nifti_data.header.set_slope_inter(None, None)
            out_nifti_data.header["descrip"] = prediction_nii_descr
            nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_liverDebug.nii.gz"))
        if modelTumorPath != None or modelTumorPatchPath != None:
            out_nifti_data = nibabel.nifti1.Nifti1Image(tumor_out_pixel_array, affine=original_affine, header=original_header.copy())
            out_nifti_data.header.set_slope_inter(None, None)
            out_nifti_data.header["descrip"] = prediction_nii_descr
            nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_tumor.nii.gz"))
        if modelNecrosisPath != None:
            out_nifti_data = nibabel.nifti1.Nifti1Image(necrosis_out_pixel_array, affine=original_affine, header=original_header.copy())
            out_nifti_data.header.set_slope_inter(None, None)
            out_nifti_data.header["descrip"] = prediction_nii_descr
            nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_necrosis.nii.gz"))


def segmentSagittalCoronal(inputFile, current_model, original_spacings, axial_liver_segmentation=None, axial_tumor_segmentation=None,
    two_5d_liver=False, two_5d_tumor=False, bg_input_masking=False, outputDir=None):
        
    print ("Resample Original volume to isotropic voxel size 0.68x0.68x0.68 ...")
    nifti_data = nibabel.load(inputFile)
    input_size = nifti_data.get_shape()
    voxel_size = [0.68, 0.68, 0.68]
    wanted_size = [int((input_size[i] * original_spacings[i])/voxel_size[i]) for i in range(3)]
    rescaled_affine = rescale_affine(nifti_data.get_affine(), voxel_dims=voxel_size)
    resampled_nifti_data = processing.resample_from_to(nifti_data, (wanted_size, rescaled_affine), order=0, mode="nearest")
    print (resampled_nifti_data.header.get("pixdim")[1:4])
    print("\n Done !")

    if type(axial_liver_segmentation) == np.ndarray :
        print ("Resample predicted liver volume to isotropic voxel size 0.68x0.68x0.68")
        pred_liver_nifti_data = nibabel.nifti1.Nifti1Image(axial_liver_segmentation, affine=nifti_data.get_affine(), header=nifti_data.header.copy())
        resampled_pred_liver_nifti_data = processing.resample_from_to(pred_liver_nifti_data, (wanted_size, rescaled_affine), order=0, mode="nearest")
        resampled_pred_liver_pixel_array = np.asanyarray(resampled_pred_liver_nifti_data.dataobj)
        del pred_liver_nifti_data

    # load file 
    # resampled_nifti_data = nibabel.load(os.path.join(tmp_dir, "out.nii.gz"))
    # resampled_pixel_array = resampled_nifti_data.get_fdata()
    resampled_pixel_array = np.asanyarray(resampled_nifti_data.dataobj)
    if resampled_nifti_data.header.has_data_slope and resampled_nifti_data.header.has_data_intercept and resampled_nifti_data.header.get_slope_inter() != (None, None):
        resampled_pixel_array = resampled_pixel_array*resampled_nifti_data.header.get_slope_inter()[0] + resampled_nifti_data.header.get_slope_inter()[1]

    rotation_matrix = Quaternion(resampled_nifti_data.header.get_qform_quaternion()).rotation_matrix
    liver_out_pixel_array_sagittal = np.zeros(resampled_pixel_array.shape)
    liver_out_pixel_array_coronal = np.zeros(resampled_pixel_array.shape)

    ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
    # for ax in range(2):
    #     if rotation_matrix[ax,ax] == 1.:
    #         resampled_pixel_array = np.flip(resampled_pixel_array, axis=ax)
            
    resampled_x_dim, resampled_y_dim, resampled_z_dim = resampled_pixel_array.shape
    print (resampled_x_dim, resampled_y_dim, resampled_z_dim)
    min_range = -100.
    max_range = 400.
    f_linFun = lambda x: (np.clip(x, min_range, max_range)-min_range)/(max_range-min_range)
    img_size = int(current_model.input[0].shape[-1]) if bg_input_masking else int(current_model.input.shape[-1])
    target = "liver" if int(current_model.output.shape[-1]) == 1 else "tumor"
    ##############################
    ### Sagittal segmentation
    ##############################
    print ("Sagittal segmentation")
    for slice_ in tqdm(range(resampled_x_dim)):
        if two_5d_liver :
            initialSagittal = [resampled_pixel_array[s, :, :] for s in [max(slice_-1, 0), slice_, min(slice_+1, resampled_x_dim-1)]]
            reshapedSagittal = [reshapeBorder(np.rot90(initialSagittal[idx]),
                borderValue=-1024, desired_size=(512, 512)) for idx in range(3)]
            X = np.array([f_linFun(reshapedSagittal[idx]) for idx in range(3)])
            out = proba2OneHot(current_model.predict(X[np.newaxis, :, :, :]), image_size=current_model.input.shape[-1])
        else:
            initialSagittal = resampled_pixel_array[slice_, :, :]
            reshapedSagittal = reshapeBorder(np.rot90(initialSagittal),
                borderValue=-1024, desired_size=(512, 512))
            if type(axial_liver_segmentation) == np.ndarray:
                initialSagittal_liverPred = resampled_pred_liver_pixel_array[slice_, :, :]
                reshapedSagittal_liverPred = reshapeBorder(np.rot90(initialSagittal_liverPred),
                    borderValue=-1024, desired_size=(512, 512))
                X = np.array([mask(f_linFun(reshapedSagittal), reshapedSagittal_liverPred)])
            else:
                X = np.array([f_linFun(reshapedSagittal)])
            out = proba2OneHot(current_model.predict(X[:, np.newaxis, :, :]), image_size=img_size, target=target)
            X = None
            reshapedSagittal = None
        out = cv2.resize(out, (512, 512), interpolation=cv2.INTER_NEAREST)
        liver_out_pixel_array_sagittal[slice_, :, :] = invertReshape(np.rot90(out, 3), desired_size=(resampled_y_dim, resampled_z_dim))
        out = None
        gc.collect()

    ##############################
    ### Coronal segmentation
    ##############################
    print ("Coronal segmentation")
    for slice_ in tqdm(range(resampled_y_dim)):
        if two_5d_liver:
            initialCoronal = [resampled_pixel_array[:, s, :] for s in [max(slice_-1, 0), slice_, min(slice_+1, resampled_y_dim-1)]]
            reshapedCoronal = [reshapeBorder(np.rot90(initialCoronal[idx]),
                borderValue=-1024, desired_size=(512, 512)) for idx in range(3)]
            X = np.array([f_linFun(reshapedCoronal[idx]) for idx in range(3)])
            out = proba2OneHot(current_model.predict(X[np.newaxis, :, :, :]), image_size=current_model.input.shape[-1])
        else:
            initialCoronal = resampled_pixel_array[:, slice_, :]
            reshapedCoronal = reshapeBorder(np.rot90(initialCoronal),
                borderValue=-1024, desired_size=(512, 512))
            if type(axial_liver_segmentation) == np.ndarray:
                initialCoronal_liverPred = resampled_pred_liver_pixel_array[:, slice_, :]
                reshapedCoronal_liverPred = reshapeBorder(np.rot90(initialCoronal_liverPred),
                    borderValue=-1024, desired_size=(512, 512))
                X = np.array([mask(f_linFun(reshapedCoronal), reshapedCoronal_liverPred)])
            else:
                X = np.array([f_linFun(reshapedCoronal)])
            out = proba2OneHot(current_model.predict(X[:, np.newaxis, :, :]), image_size=img_size, target=target)
            X = None
            reshapedCoronal = None
        out = cv2.resize(out, (512, 512), interpolation=cv2.INTER_NEAREST)
        liver_out_pixel_array_coronal[:, slice_, :] = invertReshape(np.rot90(out, 3), desired_size=(resampled_x_dim, resampled_z_dim))
        out = None
        gc.collect()



    ### Liver Segmentation Post-Processing
    # Try here to perform a 3D opening of the obtained binary volume
    struct = generate_binary_structure(3, 1)
    # struct = np.zeros((3,3))
    if type(axial_liver_segmentation) == np.ndarray:
        # liver_out_pixel_array_sagittal_debug = np.copy(liver_out_pixel_array_sagittal)
        # liver_out_pixel_array_coronal_debug = np.copy(liver_out_pixel_array_coronal)
        liver_out_pixel_array_opened = binary_opening(liver_out_pixel_array_sagittal, structure=struct, iterations=3).astype(np.int)
        # liver_out_pixel_array_sagittal_debug[np.where(np.logical_and(liver_out_pixel_array_sagittal == 1, liver_out_pixel_array_opened==0))] = 2 # Pixels labeled 2 correspond to those considered as liver and erased by the opening
        liver_out_pixel_array_sagittal[np.where(np.logical_and(liver_out_pixel_array_sagittal == 1, liver_out_pixel_array_opened==0))] = 0 # Pixels labeled 0 correspond to those considered as liver and erased by the opening
        liver_out_pixel_array_opened = None
        
        liver_out_pixel_array_opened = binary_opening(liver_out_pixel_array_coronal, structure=struct, iterations=3).astype(np.int)
        # liver_out_pixel_array_coronal_debug[np.where(np.logical_and(liver_out_pixel_array_coronal == 1, liver_out_pixel_array_opened==0))] = 2 # Pixels labeled 2 correspond to those considered as liver and erased by the opening
        liver_out_pixel_array_coronal[np.where(np.logical_and(liver_out_pixel_array_coronal == 1, liver_out_pixel_array_opened==0))] = 0 # Pixels labeled 0 correspond to those considered as liver and erased by the opening
        liver_out_pixel_array_opened = None

        liver_out_pixel_array_sagittal_debug = None
        liver_out_pixel_array_coronal_debug = None
        gc.collect()

    # get the different 3D connected components and use only the biggest
    # labeled, ncomponents = label(liver_out_pixel_array_sagittal, structure=struct)
    # _, counts = np.unique(labeled, return_counts=True)
    # biggest_connected_component = np.argsort(counts)[-2] # Biggest connected component is the bg, the second might be the liver
    # liver_out_pixel_array_debug[np.where(np.logical_and(liver_out_pixel_array_sagittal == 1, labeled!=biggest_connected_component))] = 3 # Pixels labeled 3 correspond to those considered as liver and erased by the Connected component selection
    # liver_out_pixel_array_sagittal[np.where(np.logical_and(liver_out_pixel_array_sagittal == 1, labeled!=biggest_connected_component))] = 0 # Pixels labeled 3 correspond to those considered as liver and erased by the Connected component selection

    # for ax in range(2):
    #     if rotation_matrix[ax,ax] == 1.:
    #         liver_out_pixel_array_sagittal = np.flip(liver_out_pixel_array_sagittal, axis=ax)


    ### Create dir and files 
    if not os.path.exists(os.path.abspath(outputDir)):
        os.mkdir(os.path.abspath(outputDir))

    ### Get the input filename to have more specific output name
    fname = os.path.basename(inputFile).split(".")[0] 



    out_nifti_data_sagittal = nibabel.nifti1.Nifti1Image(liver_out_pixel_array_sagittal, affine=resampled_nifti_data.get_affine(), header=resampled_nifti_data.header.copy())
    out_nifti_data_sagittal.header.set_slope_inter(None, None)
    print ("nibabel resample to initial spacing")
    out_nifti_data_sagittal = processing.resample_from_to(out_nifti_data_sagittal, (input_size, nifti_data.get_affine()), order=0, mode="nearest")
    print("\n Done !")
    nibabel.save(out_nifti_data_sagittal, os.path.join(outputDir, fname+"_liverSagittal.nii.gz"))

    out_nifti_data_coronal = nibabel.nifti1.Nifti1Image(liver_out_pixel_array_coronal, affine=resampled_nifti_data.get_affine(), header=resampled_nifti_data.header.copy())
    out_nifti_data_coronal.header.set_slope_inter(None, None)
    print ("nibabel resample to initial spacing")
    out_nifti_data_coronal = processing.resample_from_to(out_nifti_data_coronal, (input_size, nifti_data.get_affine()), order=0, mode="nearest")
    print("\n Done !")
    nibabel.save(out_nifti_data_coronal, os.path.join(outputDir, fname+"_liverCoronal.nii.gz"))
    nifti_data = None
    resampled_nifti_data = None
    liver_out_pixel_array_coronal = None
    liver_out_pixel_array_sagittal = None
    gc.collect()
    return np.asanyarray(out_nifti_data_sagittal.dataobj), np.asanyarray(out_nifti_data_coronal.dataobj)


def segmentPatchSlice(inputImage, inputSpacing, outputSpacing, model, n_classes, inputAnnotatedImage=None, stride=32,
    meanVoters=False, fixLiver=False, two_5d=False, categorical=False, toPredictPatch=[], mapping=None):
    """
        inputAnnotatedImage may be None when segmenting the liver using patches for example
        if fixLiver is set to True, the corrected liver slice is returned
    """
    transformed_image = [reshapeBorder(transform_image(inputImg.T, inputSpacing, outputSpacing), borderValue=-1024, desired_size=(512, 512)) for inputImg in inputImage]
    # transformed_image = [pre_process_image(inputImg, mapping=mapping[idx], phase="", image_size=(512, 512)) for idx, inputImg in enumerate(inputImage)]
    if inputAnnotatedImage is not None:
        transformed_annotated_image = reshapeBorder(transform_image(inputAnnotatedImage.T, inputSpacing, outputSpacing), borderValue=0, desired_size=(512, 512))
        transformed_annotated_image = transformed_annotated_image.astype("uint8")
    patch_size = int(model.input.shape[-1])
    min_range = -100.
    max_range = 400.
    f_linFun = lambda x: (np.clip(x, min_range, max_range)-min_range)/(max_range-min_range)
    # cmap = LinearSegmentedColormap.from_list('mycmap', ['black', 'blue', 'red', 'green'])
    #Tumor Patch segmentation
    # original_patches = image.extract_patches_2d(f_linFun(transformed_image), patch_size=(patch_size, patch_size))
    # transformed_image = [f_linFun(transformed_img) for transformed_img in transformed_image]
    transformed_image = [pre_process_image(transformed_image[idx], mapping=mapping[idx], phase="", image_size=512) for idx in range(len(transformed_image))]
    # segmented_patches = image.extract_patches_2d(transformed_annotated_image, patch_size=(patch_size, patch_size))
    voters = np.zeros((512,512), dtype=int) # compute how much patches predicted each pixel
    if meanVoters:
        overallPrediction = np.zeros((512, 512), dtype=int) # overall prediction
    else:
        overallPrediction = np.zeros((512, 512, n_classes), dtype=float) # overall prediction

    # need to change this is order to get only patches with at least one pixel of "Liver class"
    # toPredictPatch = nonOverlappingPatchesCoord(512, patch_size)
    # toPredictPatch = gridPatchesIdx(512, patch_size, stride)
    # outputImage = np.zeros((512,512))
    for intern_idx, patch_idx in enumerate(toPredictPatch):
        # sys.stdout.write("patch {}/{}\r".format(intern_idx, len(toPredictPatch)))
        # sys.stdout.flush()

        coordX, coordY = patchToCoord(patch_idx, 512, patch_size)
        if inputAnnotatedImage is not None:
            segmented_patch = transformed_annotated_image[coordY:coordY+patch_size, coordX:coordX+patch_size]
            if segmented_patch.max() != 1: # Consider only liver patches
                continue
        # if np.count_nonzero(segmented_patch.flatten()) > 0.25*(patch_size**2): # At least 1/4 of the pixel are non bg
        original_patch = [transformed_img[coordY:coordY+patch_size, coordX:coordX+patch_size] for transformed_img in transformed_image]
        # plt.imshow(original_patch, cmap="gray", interpolation="nearest", vmin=0.0, vmax=1.0)
        # plt.show()
        prediction = model.predict(np.array([original_patch]))
        predicted_patch = np.reshape(prediction, (patch_size, patch_size, n_classes))
        # plt.imshow(np.argmax(predicted_patch, axis=2), cmap=cmap, interpolation="nearest", vmin=0, vmax=3)
        # plt.show()
        # if n_classes == 1:
        #     outputImage[coordY:coordY+patch_size, coordX:coordX+patch_size] = predicted_patch > 0.5
        # outputImage[coordY:coordY+patch_size, coordX:coordX+patch_size] = np.argmax(predicted_patch, axis=2)
        if meanVoters:
            if n_classes > 1:
                overallPrediction[coordY:coordY+patch_size, coordX:coordX+patch_size] += np.argmax(predicted_patch, axis=2)
            else:
                predicted_patch[predicted_patch>0.5] = 1
                overallPrediction[coordY:coordY+patch_size, coordX:coordX+patch_size] += predicted_patch
        else:
            overallPrediction[coordY:coordY+patch_size, coordX:coordX+patch_size] += predicted_patch
        voters[coordY:coordY+patch_size, coordX:coordX+patch_size] += 1
        # else:
        #     voters[coordY:coordY+patch_size, coordX:coordX+patch_size] += 1
    if meanVoters:
        overallPrediction = np.around(np.divide(overallPrediction, voters))
    else:
        if n_classes > 1:
            overallPrediction = np.argmax(overallPrediction, axis=2)
        else:
            for c in range(n_classes):
                overallPrediction[:, :, c] = np.divide(overallPrediction[:, :, c], voters)
            overallPrediction = np.where(overallPrediction > 0.5, 1, 0)


    if inputAnnotatedImage is not None:
        ### ADD a cleanup step by removing all prediction where gt is bg :
        if fixLiver:
            out_image = np.zeros((512, 512), dtype=int)
            out_image[np.logical_and(transformed_annotated_image == 1, overallPrediction == 2)] = 1 # Pixels that were considered as liver and part of tumor after patch segmentation
            out_image[np.logical_and(transformed_annotated_image == 1, overallPrediction == 1)] = 2 # Pixels segmented as liver and part of the parenchyma
            out_image[np.logical_and(transformed_annotated_image == 1, overallPrediction == 0)] = 3 # Pixels that were considered as liver but now segmented as bg
            overallPrediction = out_image
        if not categorical:
            overallPrediction[overallPrediction != 2] = 0
            overallPrediction[overallPrediction == 2] = 1 # Tumor
        overallPrediction[transformed_annotated_image != 1] = 0 # Fix everything outside the predicted liver
    # plt.imshow(np.squeeze(overallPrediction))
    # plt.show()
    return overallPrediction, inputAnnotatedImage


def computeNecrosisRate (inputFile, gtTumoPath, modelNecrosisPath, gtNecrosisPath=None, outputDir=None):
    """
        Compute the ncerosis rate of the inputFile volume, with the tumor delineation given in the gtTumor volume
    """

    ### Get Raw Volume
    nifti_data = nibabel.load(inputFile)
    pixel_array = nifti_data.get_fdata()
    spacing_x, spacing_y, spacing_z = nifti_data.header.get("pixdim")[1:4]
    if nifti_data.header.has_data_slope and nifti_data.header.has_data_intercept and nifti_data.header.get_slope_inter() != (None, None):
        pixel_array = pixel_array*nifti_data.header.get_slope_inter()[0] + nifti_data.header.get_slope_inter()[1]

    necrosis_out_pixel_array = np.zeros(pixel_array.shape)
    rotation_matrix = Quaternion(nifti_data.header.get_qform_quaternion()).rotation_matrix

    ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            pixel_array = np.flip(pixel_array, axis=ax)

    ### Get Tumor Ground Truth file
    nifti_data = nibabel.load(gtTumoPath)
    tumor_gt_pixel_array = nifti_data.get_fdata()
    spacing_x, spacing_y, spacing_z = nifti_data.header.get("pixdim")[1:4]
    if nifti_data.header.has_data_slope and nifti_data.header.has_data_intercept and nifti_data.header.get_slope_inter() != (None, None):
        tumor_gt_pixel_array = tumor_gt_pixel_array*nifti_data.header.get_slope_inter()[0] + nifti_data.header.get_slope_inter()[1]

    ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            tumor_gt_pixel_array = np.flip(pixel_array, axis=ax)



    if gtNecrosisPath != None:
        ### Get Tumor Ground Truth file
        nifti_data = nibabel.load(gtNecrosisPath)
        necrosis_gt_pixel_array = nifti_data.get_fdata()
        spacing = nifti_data.header.get("pixdim")[1] # WARNING : Take only the first value of spacing : because spacing for x & y is the same and we do not consider z-spacing
        if nifti_data.header.has_data_slope and nifti_data.header.has_data_intercept and nifti_data.header.get_slope_inter() != (None, None):
            necrosis_gt_pixel_array = necrosis_gt_pixel_array*nifti_data.header.get_slope_inter()[0] + nifti_data.header.get_slope_inter()[1]

        ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
        for ax in range(2):
            if rotation_matrix[ax,ax] == 1.:
                necrosis_gt_pixel_array = np.flip(pixel_array, axis=ax)



    print ("Necrosis segmentation...\n")
    ### perform the necrosis segmentation here
    model = load_model(modelNecrosisPath, custom_objects={"cross_entropy_loss":cross_entropy_loss, "dice_loss":dice_loss, "dice_acc": dice_acc})
    min_range = -100.
    max_range = 400.
    f_linFun = lambda x: (x-min_range)/(max_range-min_range)


    tumor_size = 0
    pred_necrosis_size = 0
    true_necrosis_size = 0
    for slice_ in range(pixel_array.shape[2]):
        sys.stdout.write("{}/{}\r".format(slice_+1,pixel_array.shape[2]))
        sys.stdout.flush()

        transformed_image = cv2.resize(transform_image(pixel_array[:,:, slice_].T, (spacing_x, spacing_y), (0.97, 0.97)),
            (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
        transformed_annotated_image = cv2.resize(transform_image(tumor_gt_pixel_array[:,:, slice_].T, (spacing_x, spacing_y), (0.97, 0.97)),
            (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
        tumor_size += np.count_nonzero(transformed_annotated_image)
        if gtNecrosisPath != None:
            transformed_annotated_necrosis_image = cv2.resize(transform_image(necrosis_gt_pixel_array[:,:, slice_].T, (spacing_x, spacing_y), (0.97, 0.97)),
                (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
            true_necrosis_size += np.count_nonzero(transformed_annotated_necrosis_image)
        X = np.array([mask(f_linFun(transformed_image), transformed_annotated_image)])
        out = proba2OneHot(model.predict(X[:, np.newaxis, :, :]), image_size=model.input.shape[-1], target="tumor")
        out = cv2.resize(transform_image(out, (0.97, 0.97), (spacing_x, spacing_y)).T, (pixel_array.shape[1], pixel_array.shape[0]) , interpolation=cv2.INTER_NEAREST)
        pred_necrosis_size += np.count_nonzero(out)
        necrosis_out_pixel_array[:, :, slice_] = out

    pred_necrosis_rate = pred_necrosis_size * 100. / tumor_size
    true_necrosis_rate = true_necrosis_size * 100. / tumor_size

    print ("\n")
    print ("Number pixels tumor", tumor_size)
    print("Predicted necrosis rate", pred_necrosis_rate)

    if gtNecrosisPath!= None:
        print("True necrosis rate", true_necrosis_rate)

    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            necrosis_out_pixel_array = np.flip(necrosis_out_pixel_array, axis=ax)

    ### Create dir and files
    if not os.path.exists(os.path.abspath(outputDir)):
        os.mkdir(os.path.abspath(outputDir))

    ### Get the input filename to have more specific output name
    fname = os.path.basename(inputFile).split(".")[0]

    out_nifti_data = nibabel.nifti1.Nifti1Image(necrosis_out_pixel_array, affine=nifti_data.get_affine(), header=nifti_data.header.copy())
    out_nifti_data.header.set_slope_inter(None, None)
    nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_necrosis.nii.gz"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("niftiInputPath", nargs="+", help="Path to nifti file to segment", action="store") # nargs="+" to deal with multiphase input
    liver_group = parser.add_mutually_exclusive_group(required=True)
    liver_group.add_argument('--liver', help="model to use for the liver prediction", action="store", dest="modelLiverPath", default=None)
    liver_group.add_argument("--liver_patch", help="model to use for the liver prediction (patches version)", action="store", dest="modelLiverPathPatch", default=None)
    # parser.add_argument("modelLiverPath", help="model to use for the liver prediction", action="store")
    # parser.add_argument("--liver_patch", help="model to use for the liver prediction (patches version)", action="store")
    parser.add_argument("--tumor", help="model to use for the tumor prediction", action="store", dest="modelTumorPath", default=None)
    parser.add_argument("--tumor_patch", help="model to use for the tumor prediction (patches version)", action="store", dest="modelTumorPatchPath", default=None)
    parser.add_argument("--necrosis", help="model to use for the necrosis prediction", action="store", dest="modelNecrosisPath", default=None)
    parser.add_argument("-o", "--outputDirectory", help="directory where to store the .npy files", action="store", dest="outputDir", default=os.path.abspath(os.curdir))
    parser.add_argument("-f", "--fuse", help="option to fuse the 3 binary segmentation masks into a single", action="store_true", dest="fuseMap", default=False)
    parser.add_argument("--two_5d_liver", help="option to 2.5 input for liver network", action="store_true", dest="two_5d_liver", default=False)
    parser.add_argument("--two_5d_tumor", help="option to 2.5 input for tumor network", action="store_true", dest="two_5d_tumor", default=False)
    parser.add_argument("--bg_input_masking", help="option to mask the bg when computing the loss", action="store_true", dest="bg_input_masking", default=False)
    parser.add_argument("--triplanar", help="Perform the segmentation using only the 3-planes (Default to axial only, warning when set True: both time and memory consuming",
        action="store_true", dest="triplanar", default=False)
    # parser.add_argument("gtTumorPath", help="Volume of tumor segmentation", action="store")
    parser.add_argument("--gtNecrosis", help="Necrosis ground truth path", action="store", dest="gtNecrosisPath", default=None)
    parser.add_argument("--custom_norm_range", help="Whether or not to use custom normalisation range", action="store_true", dest="custom_norm_range", default=False)
    parser.add_argument("--mean_hist", nargs="+", help="Path to mean histograms", action="store", dest="mean_hist_files", default=[]) # nargs="+" to deal with multiphase input

    args = parser.parse_args()

    segmentNifti(args.niftiInputPath, args.modelLiverPath, args.modelLiverPathPatch, args.modelTumorPath, args.modelTumorPatchPath, args.modelNecrosisPath, args.outputDir, args.fuseMap,
        fixLiver=True, two_5d_liver=args.two_5d_liver, two_5d_tumor=args.two_5d_tumor, bg_input_masking=args.bg_input_masking, triplanar=args.triplanar, mean_hist_files=args.mean_hist_files,
        custom_norm_range=args.custom_norm_range)
    # computeNecrosisRate(args.niftiInputPath, args.gtTumorPath, args.modelNecrosisPath, gtNecrosisPath=args.gtNecrosisPath, outputDir=args.outputDir)

