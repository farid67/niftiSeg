# Installation

*Requires python 3.5 or 3.6 (seems not to work with 3.7 because of Tf)*

### With a python virtual environment

`cd <virtualEnvBaseDir>`

`python3 -m venv niftiSeg-virtEnv`

`source niftiSeg-virtEnv/bin/activate`

### Installation steps

`cd <gitSrcBaseDir>`

`git clone https://gitlab.com/farid67/niftiSeg.git`

`cd niftiSeg`

`pip3 install -r requirements.txt`



# Models download

***Use provided password***

Liver Model

https://owncloud.ircad.fr/index.php/s/52tef3IcOl6dlZT

Tumor Model

https://owncloud.ircad.fr/index.php/s/jROI1F5aaHce8ZJ

Necrosis Model

https://owncloud.ircad.fr/index.php/s/ySnu0InGuCvMq0T



# Run prediction

* Liver only 
  * `python3 niftiSeg.py <pathToFile.nii.gz> --liver <pathToLiverModel.h5>` 
* Liver tissues segmentation
  * generate 3 different volumes : `python3 niftiSeg.py <pathToFile.nii.gz> --liver <pathToLiverModel.h5> --tumor <pathToTumorModel.h5> --necrosis <pathToNecrosisModel.h5>`
  * fuse prediction in a single volume : `python3 niftiSeg.py <pathToFile.nii.gz> --liver <pathToLiverModel.h5> --tumor <pathToTumorModel.h5> --necrosis <pathToNecrosisModel.h5> -f`
* Options :
  * the output directory can also be specified using the option `-o/--outputDirectory` 
