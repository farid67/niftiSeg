from niftiSeg import proba2OneHot, reshapeBorder, invertReshape, \
gridPatchesIdx, patchToCoord, mask, fuseMasks, transform_image
import numpy as np
import unittest

class niftiSegTest(unittest.TestCase):

    def test_proba2OneHot(self):
        # Zeros
        proba = np.zeros((2,2), dtype=np.float)
        out = proba2OneHot(proba, image_size=2, target="liver")
        np.testing.assert_array_equal(out, np.zeros((2,2), dtype="int"))

        # Ones
        proba = np.ones((2,2), dtype=np.float)
        out = proba2OneHot(proba, image_size=2, target="liver")
        np.testing.assert_array_equal(out, np.ones((2,2), dtype="int"))

        # Tumor Zeros
        proba = np.zeros((2,2,3), dtype=np.float)
        proba[:,:,0] = 1. # bg
        out = proba2OneHot(proba, image_size=2, target="tumor")
        np.testing.assert_array_equal(out, np.zeros((2,2), dtype="int"))


        # Tumor Ones
        proba = np.zeros((2,2,3), dtype=np.float)
        proba[:,:,2] = 1. # bg
        out = proba2OneHot(proba, image_size=2, target="tumor")
        np.testing.assert_array_equal(out, np.ones((2,2), dtype="int"))

        # Unknown target when transforming the probabilities
        self.assertRaises(Exception, proba2OneHot, proba, 512, "bg")

    def test_transformImage(self):
        inputImg = np.zeros((100, 100))
        out = transform_image(inputImg, (0.5, 0.5), (1.0, 1.0))
        self.assertEqual(out.shape, (50, 50))

        out = transform_image(inputImg, (0.5, 1.0), (1.0, 1.0))
        self.assertEqual(out.shape, (50, 100))

        out = transform_image(inputImg, (1.0, 1.0), (0.5, 0.5))
        self.assertEqual(out.shape, (200, 200))

        out = transform_image(inputImg, (0.5, 1.0), (0.5, 0.5))
        self.assertEqual(out.shape, (100, 200))

    def test_reshapeBorder(self):
        # desired_size[0] > inSize[0]
        inputImg = np.zeros((100, 100))
        out = reshapeBorder(inputImg, 1, (101, 100))
        np.testing.assert_array_equal(out.shape, (101, 100))

        # desired_size[1] > inSize[1]
        inputImg = np.zeros((100, 100))
        out = reshapeBorder(inputImg, 1, (100, 101))
        np.testing.assert_array_equal(out.shape, (100, 101))

        # desired_size[0] < inSize[0]
        inputImg = np.zeros((100, 100))
        out = reshapeBorder(inputImg, 1, (99, 100))
        np.testing.assert_equal(out.shape, (99, 100))

        # # desired_size[1] < inSize[1]
        inputImg = np.zeros((100, 100))
        out = reshapeBorder(inputImg, 1, (100, 99))
        np.testing.assert_equal(out.shape, (100, 99))
        
    def test_InvertReshapeBorder(self):
        # desired_size[0] > inSize[0]
        inputImg = np.zeros((100, 100))
        out = invertReshape(inputImg, (101, 100))
        np.testing.assert_array_equal(out.shape, (101, 100))

        # desired_size[1] > inSize[1]
        inputImg = np.zeros((100, 100))
        out = invertReshape(inputImg, (100, 101))
        np.testing.assert_array_equal(out.shape, (100, 101))

        # desired_size[0] < inSize[0]
        inputImg = np.zeros((100, 100))
        out = invertReshape(inputImg, (99, 100))
        np.testing.assert_equal(out.shape, (99, 100))

        # # desired_size[1] < inSize[1]
        inputImg = np.zeros((100, 100))
        out = invertReshape(inputImg, (100, 99))
        np.testing.assert_equal(out.shape, (100, 99))
        
    def test_reshapeAndInvert(self):
        """
            A = invertReshape(reshapeBorder(A))
        """
        inputImg = np.zeros((100, 100))
        out = invertReshape(reshapeBorder(inputImg, 1, desired_size=(100,150)), inputImg.shape)
        np.testing.assert_equal(inputImg, out)

        inputImg = np.zeros((100, 100))
        out = invertReshape(reshapeBorder(inputImg, 1, desired_size=(150,100)), inputImg.shape)
        np.testing.assert_equal(inputImg, out)

    def test_gridPatchesIdx(self):
        idxs = gridPatchesIdx(64, 32, 32)
        self.assertEqual(len(idxs), 4)
        coords = [patchToCoord(idx, 64, 32) for idx in idxs]
        self.assertIn((0,0), coords)
        self.assertIn((32,0), coords)
        self.assertIn((0,32), coords)
        self.assertIn((32,32), coords)

    def test_mask(self):
        # All pixels classified as Bg
        inputImg = np.zeros((100, 100), dtype=np.float)
        gtImg = np.zeros((100, 100))
        out = mask(inputImg, gtImg)
        np.testing.assert_equal(out, np.ones((100, 100))*-1)

        # All pixels classified as nonBg
        inputImg = 100 * np.ones((100, 100), dtype=np.float)
        gtImg = np.ones((100, 100))
        out = mask(inputImg, gtImg)
        np.testing.assert_equal(out, np.ones((100, 100))*100)

    def test_fuseMaks(self):
        liverBinary = np.zeros((1, 100, 100))
        liverBinary[:, 0:30, :] = 1
        tumorBinary = np.zeros((1, 100, 100))
        tumorBinary[:, 30:60, :] = 1
        necrosisBinary = np.zeros((1, 100, 100))
        necrosisBinary[:, 60:, :] = 1

        out = fuseMasks(liverBinary=liverBinary,
        tumorBinary=tumorBinary, necrosisBinary=necrosisBinary)

        wantedOut = np.zeros((1, 100, 100))
        wantedOut[:, 0:30, :] = 1
        wantedOut[:, 30:60, :] = 2
        wantedOut[:, 60:, :] = 3

        np.testing.assert_equal(out, wantedOut)


if __name__ == "__main__":
    unittest.main()